#!/usr/bin/env bash

export THISDIR=$(readlink -f $(dirname $BASH_SOURCE))
export CUSTOMCONDA="$THISDIR/customconda"
# Remove existing conda, if any, and pre-pend this conda
export PATH="$CUSTOMCONDA/bin:$(echo $PATH | tr ':' '\n' | grep -v conda | tr '\n' ':')"
# No matter what I try (env variables, overriding files, etc.) conda still
# wants to load things like channels from ~/.condarc.  This prevents conda from
# looking elsewhere, though.
export HOME="$THISDIR"
export TMPDIR="$THISDIR/tmp"
mkdir -p "$TMPDIR"
# Run the given arguments
if (( $# != 0 )); then
	"$@"
fi
