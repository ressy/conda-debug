#!/usr/bin/env bash
src=Miniconda3-latest-Linux-x86_64.sh
if [ ! -e "$src" ]; then
	wget "https://repo.anaconda.com/archive/$src" || wget "https://repo.anaconda.com/miniconda/$src"
fi
bash "$src" -b -p "$CUSTOMCONDA"
conda init bash
