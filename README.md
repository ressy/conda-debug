# Conda Debug Wrapper Script

    $ env -i bash --noprofile --norc
    $ ./conda_wrapper.sh conda_setup.sh
    $ ./conda_wrapper.sh conda ...
